package edu.mapreduce.dto;

public class BidDto {

    private String bidId;
    private Integer bidPrice;
    private Integer countOfBidPrice;

    public BidDto() {
    }

    public BidDto(String bidId, Integer bidPrice, Integer countOfBidPrice) {
        this.bidId = bidId;
        this.bidPrice = bidPrice;
        this.countOfBidPrice = countOfBidPrice;
    }

    public String getBidId() {
        return bidId;
    }

    public void setBidId(String bidId) {
        this.bidId = bidId;
    }

    public Integer getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(Integer bidPrice) {
        this.bidPrice = bidPrice;
    }

    public Integer getCountOfBidPrice() {
        return countOfBidPrice;
    }

    public void setCountOfBidPrice(Integer countOfBidPrice) {
        this.countOfBidPrice = countOfBidPrice;
    }
}

